'use strict';

var _ = require('lodash');

var dependencies = {}; /* It's some kind of singletone */

function register(name, object) {
  if (!(name && object))
    throw new Error('Bad args');

  if (dependencies[name])
    throw new Error('Name has been already reserved');

  dependencies[name] = object;
}

function remove(name) {
  delete dependencies[name];
}

function get(deps, override) {
  function _get(name) {
    if (override && override[name])
      return override[name];
    
    var item = dependencies[name];
    if (!item)
      throw new Error('unresolved dependency: ' + name);

    return item;
  }

  if (typeof deps === 'string')
    return _get(deps);

  return _.map(deps, _get);
}

function getFunction(func, override) {
  var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/g;
  var ARGUMENT_NAMES = /([^\s,]+)/g;
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var args = fnStr
    .slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')'))
    .match(ARGUMENT_NAMES);

  if (args === null)
    return func();

  return func.apply(null, get(args, override));
}

function getString(name, override) {
  if (override)
    return override;
  return get(name);
}

function getArray(array, override) {
  return get(array, override);
}

function resolve(object, override) {
  if (override && !_.isPlainObject(override))
    throw new Error('Plain object with override definitions is expected');

  if (_.isFunction(object))
    return getFunction(object, override);
  else if (_.isString(object))
    return getString(object, override);
  else if (_.isArray(object))
    return getArray(object, override);
  else
    return null;
}

function payload() {
  return dependencies;
}

module.exports = {
  register: register,
  resolve: resolve,
  remove: remove,
  payload: payload
};

